import dynamic from 'next/dynamic';

const CKEditor = dynamic(() => import('@ckeditor/ckeditor5-react'), { ssr: false });
const CKEditorClassic = dynamic(() => import('@ckeditor/ckeditor5-build-classic'), {
  ssr: false
});

export default { CKEditor, CKEditorClassic };
