const CommentPost = () => {
  return (
    <div className="flex text-gray-800 mb-8">
      <div className="mt-2">
        <div className="w-[36px]">
          <img src="/comment-pp.png" />
        </div>
      </div>
      <div className="mr-4"></div>
      <div className="">
        <p className="text-lg font-semibold">Sarah Venella</p>
        <p className="text-sm  my-2">
          Lorem ipsum dolor sit amet, ea sed illud facilis percipit, errem euripidis vim
          ne. Esse feugiat perfecto nec ei
        </p>
        <p className="text-sm flex text-gray-500">
          <p className="mr-4">12 minutes ago</p>
          <p className="flex items-center ">
            <svg
              className="w-4 h-4 mr-2"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"
              ></path>
            </svg>{' '}
            0
          </p>
        </p>
      </div>
    </div>
  );
};

export default CommentPost;
