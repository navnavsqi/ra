import { useState } from 'react';
import { toast } from 'react-toastify';
import vars from 'vars';

const FormCategory = ({ token }) => {
  const [categoryName, setCategoryName] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log('categoryName', JSON.stringify({ categoryName }));
    // form data no need to specify multipart/form-data in headers
    const apiRes = await fetch(`${vars.API_LOCAL}api/v1/categories`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify({ categoryName })
    });

    const data = await apiRes.json();

    if (apiRes.ok) {
      console.log(data);
      toast.info('Category berhasil ditambahkan', {
        position: toast.POSITION.TOP_CENTER
      });
      setTimeout(() => {
        window.location.reload();
      }, 1100);
    } else {
      toast.error(data.message, {
        position: toast.POSITION.TOP_CENTER
      });

      return false;
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="form">
        <label>Category</label>
        <div className="form-control flex-1 flex my-1">
          <input
            type="text"
            className="border border-gray-300 p-2 text-sm w-full box-borderfocus:border-gray-200 focus:outline-none"
            placeholder="Category Name"
            onChange={(e) => setCategoryName(e.target.value)}
          />
          <button className="border bg-primary py-2 px-5 text-white ml-4">Add</button>
        </div>
      </div>
    </form>
  );
};

export default FormCategory;
