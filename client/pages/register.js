import cookie from 'cookie';
import { useRouter } from 'next/router';
import { useState } from 'react';
import { toast } from 'react-toastify';
import vars from 'vars';
import { Layout } from './components/molecules';

const Register = () => {
  const [user, setUser] = useState(null);
  const [error, setError] = useState(null);
  const [register, setRegister] = useState({
    email: '',
    password: '',
    name: '',
    phoneNumber: ''
  });
  const router = useRouter();

  console.log(register);

  const registerAct = async ({ email, password }) => {
    const res = await fetch(`/api/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(register)
    });

    const data = await res.json();

    if (res.ok) {
      console.log(data);
      localStorage.setItem('user', JSON.stringify(data.user));
      setUser(data.user);
      router.replace('/cms');
    } else {
      toast.error(data.message, {
        position: toast.POSITION.TOP_CENTER
      });
      setError(data.message);
      // setError(null);
    }
  };

  const handleChange = (e) => {
    setRegister({
      ...register,
      [e.target.name]: e.target.value
    });
  };

  const handleRegister = (e) => {
    e.preventDefault();
    registerAct(register);
  };

  return (
    <>
      <Layout title="Designate — Platform media kolaboratif">
        <div className="container mx-auto px-4 h-full">
          <div className="flex content-center items-center justify-center h-full">
            <div className="w-full lg:w-4/12 px-4 my-20">
              <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-100 border-0">
                <div className="rounded-t mb-0 px-6 py-6">
                  <div className="text-center mb-3">
                    <h3 className="text-gray-600 text-lg font-bold">Register</h3>
                  </div>

                  <hr className="mt-6 border-b-1 border-gray-400" />
                </div>
                <div className="flex-auto px-4 lg:px-10 py-10 pt-0">
                  <div className="text-gray-500 text-center mb-3 font-bold"></div>
                  <form onSubmit={handleRegister}>
                    <div className="relative w-full mb-3">
                      <label
                        className="block uppercase text-gray-700 text-xs font-bold mb-2"
                        htmlFor="grid-password"
                      >
                        Name
                      </label>
                      <input
                        type="text"
                        className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                        placeholder="Your Name"
                        style={{ transition: 'all .15s ease' }}
                        name="name"
                        onChange={handleChange}
                      />
                      <p className="text-red-500 text-xs"></p>
                    </div>

                    <div className="relative w-full mb-3">
                      <label
                        className="block uppercase text-gray-700 text-xs font-bold mb-2"
                        htmlFor="grid-password"
                      >
                        Email
                      </label>
                      <input
                        type="email"
                        className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                        placeholder="Email"
                        style={{ transition: 'all .15s ease' }}
                        name="email"
                        onChange={handleChange}
                      />
                      <p className="text-red-500 text-xs"></p>
                    </div>

                    <div className="relative w-full mb-3">
                      <label
                        className="block uppercase text-gray-700 text-xs font-bold mb-2"
                        htmlFor="grid-password"
                      >
                        Phone Number
                      </label>
                      <input
                        type="text"
                        className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                        placeholder="Phone Number"
                        style={{ transition: 'all .15s ease' }}
                        name="phoneNumber"
                        onChange={handleChange}
                      />
                      <p className="text-red-500 text-xs"></p>
                    </div>

                    <div className="relative w-full mb-3">
                      <label
                        className="block uppercase text-gray-700 text-xs font-bold mb-2"
                        htmlFor="grid-password"
                      >
                        Password
                      </label>
                      <input
                        type="password"
                        className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                        placeholder="Password"
                        style={{ transition: 'all .15s ease' }}
                        name="password"
                        onChange={handleChange}
                      />
                      <p className="text-red-500 text-xs"></p>
                    </div>

                    <div className="text-center mt-6">
                      <button
                        className="bg-primary text-white active:bg-gray-700 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
                        type="submit"
                        style={{ transition: 'all .15s ease' }}
                      >
                        Sign In
                      </button>
                    </div>
                  </form>
                  <div className="flex flex-wrap mt-6">
                    <div className="text-center mx-auto">
                      <a href="register" className="text-gray-900">
                        <small>Create new account</small>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </>
  );
};

export default Register;

export async function getServerSideProps({ req, res }) {
  if (!req.headers.cookie) {
    return {
      props: {}
    };
  }

  const cookies = cookie.parse(req.headers.cookie);
  // some auth logic here
  const apiRes = await fetch(`${vars.API_LOCAL}api/v1/users/me`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${cookies.token}`
    }
  });

  const data = await apiRes.json();

  if (data?.user?.id) {
    return {
      redirect: {
        destination: '/cms',
        permanent: false
      }
    };
  }

  return {
    props: {} // will be passed to the page component as props
  };
}
