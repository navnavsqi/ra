'use strict';
const slugify = require('slugify');

const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Article extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Article.belongsTo(models.Category, { foreignKey: 'categoryId', as: 'category' });
    }
  }
  Article.init(
    {
      categoryId: DataTypes.NUMBER,
      title: DataTypes.STRING,
      slug: {
        type: DataTypes.VIRTUAL,
        get() {
          const rawVal = this.getDataValue('title');
          return slugify(rawVal, { lower: true });
        },
        set(value) {
          throw new Error('Do not try to set the `fullName` value!');
        }
      },
      image: DataTypes.STRING,
      desc: DataTypes.TEXT,
      isActive: DataTypes.BOOLEAN
    },
    {
      sequelize,
      modelName: 'Article',
      tableName: 'articles'
    }
  );
  return Article;
};
