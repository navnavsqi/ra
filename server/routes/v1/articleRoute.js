const express = require('express');
const router = express.Router();
const articleController = require('../../controllers/articleController');
const auth = require('../../middlewares/auth');
const files = require('../../utils/files');

router
  .route('/')
  .get(articleController.getArticles)
  .post(
    files.uploadSingle('articles', 'image', './public/images/articles'),
    articleController.createArticle
  );

router
  .route('/:id')
  .get(articleController.getOneArticle)
  .delete(auth('admin'), articleController.deleteArticle);

module.exports = router;
